$(document).ready(function(){
    var wrapper = $(".telephone");
	var add_button = $(".add_tel");

    $('#modal').on('shown.bs.modal', function () {});
    
    $('.btn-updated').hide();
    
    $('.listTelephone').popover({
        title: 'Telefones'
    });

    $('#imageUploadForm').on('submit',(function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);

        $.ajax({
            type: "POST",
            url: 'profile',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.success) {
                alertify.notify(response.message, 'success', 5);
                $("#img").attr("src",response.src);
                $("#img-app").attr("src",response.src);
            }
        }).fail(function(response){
            alertify.error(response.message);
            console.log(response.error);
        });

    }));

    $(".btn-upload").on("click", function() {
        $("#imageUploadForm").submit();
    });

    /**
     * Btn edit
     */
    $('.btn-edit').click(function (){        
        $(".remove_field").parent('div').remove();
        $('.btn-save').hide();
        $('.btn-updated').show();

        $.ajax({
            type: "POST",
            url: 'showContact',
            data: {
                schedule_contact_id: $(this).data('contact-id-edit'),
                _token: $('#_token').val(),
            }
        }).done(function (response) {
            if (response.success) {
                $('#schedule_contact_id').val(response.data.id);
                $('#schedule_contact_name').val(response.data.name).focus();
                $('#schedule_contact_surname').val(response.data.surname).focus();
                $('#schedule_contact_email').val(response.data.email).focus();
                $('#telone').val(response.data.telephones[0].toString()).focus();

                for (let i = 1; i < response.data.telephones.length; i++) {
                    $(wrapper).append('<div class="form-group" style="margin-top: 10px;"><label>Telefone '+i+'</label><input name="telephone_number[]" value="'+response.data.telephones[i].toString()+'" type="text" class="form-control"><a href="#" class="remove_field"></a></div>');
                }
                
            }
        }).fail(function(response){
            console.log(response.error);
        });
    });

    $(add_button).on("click", function(e){
		e.preventDefault();
        $(wrapper).append('<div class="form-group" style="margin-top: 10px;"><label>Telefone <span class="badge badge-warning">novo</span></label><input name="telephone_number[]" type="tel" class="form-control"><a href="#" class="remove_field btn btn-danger">x</a></div>');
	});
	$(wrapper).on("click",".remove_field", function(e){
		e.preventDefault(); $(this).parent('div').remove();
    });

    /**
     * Btn save
     */
    $('.btn-save').click(function (e){
        e.preventDefault(); actionForm($(this).data('url-save'));
    });

    /**
     * Btn update
     */
    $('.btn-updated').click(function (e){
        e.preventDefault(); actionForm($(this).data('url-update'));
    });

    /**
     * Btn add
     */
    $('.btn-add').click(function (){
        $(".remove_field").parent('div').remove();
        $('.btn-updated').hide();
        $('.btn-save').show();
        $('#schedule_contact_id').val('').focus();
        $('#schedule_contact_name').val('');
        $('#schedule_contact_surname').val('');
        $('#schedule_contact_email').val('');
        $('#telone').val('');
    });

    /**
     * Btn Excluir
     */
    $('.btn-excluir').click(function (){
        let schedule_contact_id = $(this).data('contact-id-delete');
        let token = $(this).data('token');
        if(confirm("Deseja deletar este registro ?")){
            $.ajax({
                type: "POST",
                url: 'deleteContact',
                data: {
                    schedule_contact_id: schedule_contact_id,
                    _token: token,
                }
            }).done(function (response) {
                if (response.success) {
                    alertify.notify(response.message, 'success', 5);
                    location.reload();
                }
            }).fail(function(response){
                alertify.error(response.message);
                console.log(response.error);
            });
        }
    });

    function actionForm(url) {
        let telephones = [];
        let input = document.getElementsByName('telephone_number[]'); 
        let dados = {
            _token: $('#_token').val(),
            schedule_contact_id: $('#schedule_contact_id').val(),
            schedule_contact_name: $('#schedule_contact_name').val(),
            schedule_contact_surname: $('#schedule_contact_surname').val(),
            schedule_contact_email: $('#schedule_contact_email').val(),
            telephone_number: telephones,
        };

        for (let i = 0; i < input.length; i++) {
            telephones.push(input[i].value);
        } 

        $.ajax({
            type: "POST",
            url: url,
            data: dados
        }).done(function (response) {
            if (response.success) {
                alertify.notify(response.message, 'success', 5);
                location.reload();
            }
        }).fail(function(response){
            alertify.notify(response.message, 'error', 5);
            console.log(response.error);
        });
    }

});