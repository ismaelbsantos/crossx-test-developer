@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Meu Perfil</div>

                <div class="card-body">
                    <img id="img" src="/uploads/images/{{$user->image}}" style="width: 150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px;"/>
                    <h2>{{ $user->name}}</h2>
                    <form id="imageUploadForm" enctype="multipart/form-data" action="" method="post">
                        <input id="_token" name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <input type="file" name="image" id="image"/>
                        <button type="button" class="btn btn-info float-right btn-upload">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
