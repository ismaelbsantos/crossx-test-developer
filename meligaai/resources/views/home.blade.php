@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Lista de Contatos</div>
                <div class="card-body">
                    <a href="#" class="btn btn-link btn-add" data-toggle="modal" data-target="#modal">
                        Adicionar novo contato
                    </a>
                    <table id="table_contact" class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th>Nome</th>
                                <th>Sobrenome</th>
                                <th>E-mail</th>
                                <th>Telefone</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contacts as $contact)
                                <tr>
                                    <td>{{$contact['name']}}</td>
                                    <td>{{$contact['surname']}}</td>
                                    <td>{{$contact['email']}}</td>
                                    <td>
                                        @foreach ($contact['telephones'] as $telephone)
                                            {{$telephone}} @php break; @endphp
                                        @endforeach
                                        @php 
                                            if (count($contact['telephones']) > 1) {
                                                array_shift($contact['telephones']);
                                        @endphp
                                            <i class="material-icons listTelephone" style="cursor:pointer;" data-toggle="popover" data-content="{{implode(',', $contact['telephones'])}}">list</i>
                                        @php } @endphp
                                    </td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#modal" class="btn btn-link btn-edit" data-contact-id-edit="{{$contact['id']}}">Editar</a>
                                        <a href="#" class="btn btn-link btn-excluir" data-contact-id-delete="{{$contact['id']}}" data-token="{{ csrf_token() }}">Excluir</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modal');
@endsection
