<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Novo contato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      <form id="form" action="" method="post">
            <input id="_token" name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <input id="schedule_contact_id" name="schedule_contact_id" type="hidden"/>
            <div class="form-group">
                <label for="schedule_contact_name">Nome</label>
                <input name="schedule_contact_name" id="schedule_contact_name" type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="schedule_contact_surname">Sobrenome</label>
                <input name="schedule_contact_surname" id="schedule_contact_surname" type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="schedule_contact_email">Email</label>
                <input name="schedule_contact_email" id="schedule_contact_email" type="email" class="form-control">
            </div>
            <div class="form-group">
                <div class="telephone">
                    <label for="telephone_number">Telefone principal</label>
                    <input name="telephone_number[]" id="telone" type="text" class="form-control">
                    <a href="#" class="btn btn-primary add_tel">+</a>
                </div>
            </div>
            <hr>
            <button class="btn btn-success btn-save float-right" type="submit" data-url-save="saveContact">Salvar</button>
            <button class="btn btn-info btn-updated float-right" type="submit" data-url-update="updateContact">Atualizar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </form>


      </div>
    </div>
  </div>
</div>