## Sobre o projeto

Estou projeto foi criando com a versão do Laravel 5.6 utilizando algumas bibliotecas externas que será descrita logo abaixo. Também foi utilizado XAMP e o banco de dados MYSQL.

- Krvole Eloquent Model Generator. Link: https://github.com/krlove/eloquent-model-generator
- Kitloong Laravel Migrations Generator. Link: https://github.com/kitloong/laravel-migrations-generator
- Intervention Image. link: http://image.intervention.io/

## Rodando o projeto

Baixar o XAMPP em sua versão do PHP 7.4.13. Link: https://www.apachefriends.org/pt_br/download.html
Após instalar o servidor, fazer o clone do repositório, e colocar dentro da pasta htdocs do xamp.

- Coloque o MYSQL para ativar e crie o database: meligaai.

Dentro da pasta do projeto executar o composer install, logo após execute o comando php artisan migrate para gerar as tabelas na base de dados.

Após criar as tabelas na base de dados, execute o comando php artisan db:seed para criar um usuário de teste para acessar o sistema.

Dados de acesso:
E-mail: master@gmail.com
Senha: secreta

Por fim, execute o comando php artisan serve e acesse o projeto.