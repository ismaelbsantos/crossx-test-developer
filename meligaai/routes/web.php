<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('profile', 'HomeController@profile');
Route::post('profile', 'HomeController@uploadImage');

Route::post('deleteContact', 'ScheduleContactController@delete');
Route::post('showContact', 'ScheduleContactController@show');
Route::post('saveContact', 'ScheduleContactController@create');
Route::post('updateContact', 'ScheduleContactController@update');