<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelephoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telephone', function (Blueprint $table) {
            $table->increments('telephone_id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('telephone_number', 15)->nullable()->unique('telephone_number_UNIQUE');
            $table->unsignedInteger('schedule_contact_schedule_contact_id')->index('fk_telephone_schedule_contact_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telephone');
    }
}
