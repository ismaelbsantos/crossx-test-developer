<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_contact', function (Blueprint $table) {
            $table->increments('schedule_contact_id');
            $table->string('schedule_contact_name', 45)->nullable();
            $table->string('schedule_contact_surname', 45)->nullable();
            $table->string('schedule_contact_email', 45)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->unsignedInteger('users_id')->index('fk_schedule_contact_users1_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_contact');
    }
}
