<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTelephoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telephone', function (Blueprint $table) {
            $table->foreign('schedule_contact_schedule_contact_id', 'fk_telephone_schedule_contact')->references('schedule_contact_id')->on('schedule_contact')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telephone', function (Blueprint $table) {
            $table->dropForeign('fk_telephone_schedule_contact');
        });
    }
}
