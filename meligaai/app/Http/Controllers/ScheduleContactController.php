<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Telephone;
use App\Models\ScheduleContact;
use Auth;
use Exception;

class ScheduleContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {
        $contacts = [];
        $contatos = ScheduleContact::where('deleted_at', null)
                                    ->where('users_id', Auth::user()->id)
                                    ->get();
        foreach ($contatos as $contato) {
            $contacts[$contato->schedule_contact_id] = [
                "id" => $contato->schedule_contact_id,
                "name" => $contato->schedule_contact_name,
                "surname" => $contato->schedule_contact_surname,
                "email" => $contato->schedule_contact_email,
            ];
            
            $telefones = Telephone::where('schedule_contact_schedule_contact_id', $contato->schedule_contact_id)->get();
            foreach ($telefones as $telefone) {
                $contacts[$contato->schedule_contact_id]["telephones"][] = $telefone->telephone_number;
            }
        }

        return $contacts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try
        {
            $dados = $request->all();

            $schedule = new ScheduleContact();
            $schedule->schedule_contact_name = $dados['schedule_contact_name'];
            $schedule->schedule_contact_surname = $dados['schedule_contact_surname'];
            $schedule->schedule_contact_email = $dados['schedule_contact_email'];
            $schedule->users_id = Auth::user()->id;
            $schedule->save();

            foreach ($dados['telephone_number'] as $number)
            {
                $telefone = new Telephone();
                $telefone->telephone_number = $number;
                $telefone->schedule_contact_schedule_contact_id = $schedule->schedule_contact_id;
                $telefone->save();
            }

            return response()->json(array('success' => true, "message" => 'Registro inserido com sucesso'), 200);
        }catch(Exception $e)
        {
            return response()->json(array('fail' => true, "message" => 'Falha ao inserir o registro', "error" => $e->getMessage()), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try
        {
            $contacts = [];
            $dados = $request->all();
            
            $contatos = ScheduleContact::where('schedule_contact_id', $dados['schedule_contact_id'])->first();
            $contacts = [
                "id" => $contatos->schedule_contact_id,
                "name" => $contatos->schedule_contact_name,
                "surname" => $contatos->schedule_contact_surname,
                "email" => $contatos->schedule_contact_email,
            ];
            
            $telefones = Telephone::where('schedule_contact_schedule_contact_id', $contatos->schedule_contact_id)->get();
            foreach ($telefones as $telefone) 
            {
                $contacts["telephones"][] = $telefone->telephone_number;
            }

            return response()->json(array('success' => true, 'data' => $contacts), 200);
        }catch(Exception $e)
        {
            return response()->json(array('fail' => true, "error" => $e->getMessage()), 500);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
        {
            $dados = $request->all();
            
            $schedule = ScheduleContact::find($dados['schedule_contact_id']);
            $schedule->schedule_contact_name = $dados['schedule_contact_name'];
            $schedule->schedule_contact_surname = $dados['schedule_contact_surname'];
            $schedule->schedule_contact_email = $dados['schedule_contact_email'];
            $schedule->save();

            $telephones = Telephone::where('schedule_contact_schedule_contact_id', $dados['schedule_contact_id'])->get();
            foreach ($telephones as $key => $telephone)
            {
                if (!empty($dados['telephone_number'][$key])) {
                    $tel = Telephone::find($telephone->telephone_id);
                    $tel->telephone_number = $dados['telephone_number'][$key];
                    $tel->save();
                }
            }

            return response()->json(array('success' => true, "message" => 'Registro atualizado com sucesso'), 200);
        }catch(Exception $e)
        {
            return response()->json(array('fail' => true, "message" => 'Falha ao atualizar registro', "error" => $e->getMessage()), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try
        {
            $contact = $request->all();
            ScheduleContact::where('schedule_contact_id', $contact['schedule_contact_id'])
                            ->where('users_id', Auth::user()->id)
                            ->delete();
            return response()->json(array('success' => true, "message" => 'Registro deletado com sucesso'), 200);
        }catch(Exception $e)
        {
            return response()->json(array('fail' => true, "message" => 'Falha ao deletar o registro', "error" => $e->getMessage()), 500);
        }
    }
}
