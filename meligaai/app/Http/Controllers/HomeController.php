<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use Exception;
use App\User;
use App\Http\Controllers\ScheduleContactController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = ScheduleContactController::index();
        return view('home', compact('contacts'));
    }

    public function uploadImage(Request $request)
    {
        try{
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = time().'.'.$image->getClientOriginalExtension();
                Image::make($image)->resize(300, 300)->save(public_path('/uploads/images/'.$filename));

                $user = Auth::user();
                $user->image = $filename;
                $user->save();

                $user = User::where('id', $user->id)->first();
            }
            return response()->json(array('success' => true, "message" => 'Imagem alterada com sucesso', 'src' => '/uploads/images/'.$user->image), 200);
        }catch(Exception $e)
        {
            return response()->json(array('fail' => true, "message" => 'Falha ao fazer upload', "error" => $e->getMessage()), 500);
        }
        
    }

    public function profile()
    {
        return view('profile', ['user' => Auth::user()]);
    }
}
