<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $telephone_id
 * @property int $schedule_contact_schedule_contact_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $telephone_number
 * @property ScheduleContact $scheduleContact
 */
class Telephone extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'telephone';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'telephone_id';

    /**
     * @var array
     */
    protected $fillable = ['schedule_contact_schedule_contact_id', 'created_at', 'updated_at', 'deleted_at', 'telephone_number'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scheduleContact()
    {
        return $this->belongsTo('App\ScheduleContact', 'schedule_contact_schedule_contact_id', 'schedule_contact_id');
    }
}
