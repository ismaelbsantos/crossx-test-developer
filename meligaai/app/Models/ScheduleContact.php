<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $schedule_contact_id
 * @property int $users_id
 * @property string $schedule_contact_name
 * @property string $schedule_contact_surname
 * @property string $schedule_contact_email
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Telephone[] $telephones
 */
class ScheduleContact extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'schedule_contact';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'schedule_contact_id';

    /**
     * @var array
     */
    protected $fillable = ['users_id', 'schedule_contact_name', 'schedule_contact_surname', 'schedule_contact_email', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function telephones()
    {
        return $this->hasMany('App\Telephone', 'schedule_contact_schedule_contact_id', 'schedule_contact_id');
    }
}
